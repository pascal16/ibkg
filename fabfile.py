from fabric.api import *

path = '/srv/ibkg'

def prod():
  env.user = 'root'
  env.hosts = ['screenfic.com']

def pull():
  with cd(path):
    run('git pull origin master')

# def download():
#   with cd(path):
#     run('bin/python twitter_api.py')

def deploy():
  with cd(path):
    run('supervisorctl stop ibkg')
    run('git pull origin master')
    with cd(path + "/static"):
      run('grunt build')
    run('supervisorctl start ibkg')

def restart():
  env.user = 'root'
  env.hosts = ['screenfic.com']
  with cd(path):
    run('supervisorctl restart ibkg')
