import logging
from datetime import datetime
from copy import deepcopy
import json
import hashlib
import uuid

import sqlalchemy as sql
from sqlalchemy import Table, Column, Integer, String, DateTime, ForeignKey, Boolean, Float, Text
from sqlalchemy import orm, func, desc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property

from db_config import *

from server import session

engine = sql.create_engine(connection_string, pool_recycle=3600)

Base = declarative_base()
Base.metadata.bind = engine

class MyMixin(object):
  def to_dict(self):
    d = deepcopy(self.__dict__)
    del d['_sa_instance_state']
    return d

###################################
# Relations
###################################

class Author(Base, MyMixin):
  __tablename__                   = 'author'
  name                            = Column(String(200), primary_key=True)
  change_org_url                  = Column(String(200), nullable=True)

  def __repr__(self):
    return "%s" % (self.name)

class ChangeOrgReason(Base, MyMixin):
  __tablename__                   = 'change_org_reason'
  id                              = Column(Integer, primary_key=True)
  content                         = Column(Text, nullable=False)
  created_at                      = Column(DateTime, nullable=False)
  like_count                      = Column(Integer, default=0)
  author_name                     = Column(String(200), ForeignKey('author.name'), nullable=False)
  author                          = orm.relationship(Author, backref=orm.backref("reasons", uselist=True))

  def __repr__(self):
    return "%s" % (self.content)


###################################
# END Relations
###################################

Base.metadata.create_all()
logging.info("Table information loaded")

# session.add(Author(nickname="Pascal"))
# session.commit()
# pascal = session.query(Author).filter(Author.nickname == "Pascal")[0]
#story = session.query(Story).all()
#import pdb; pdb.set_trace()
