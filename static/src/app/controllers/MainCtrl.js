/*global _, moment*/

function MainCtrl($scope, ReasonService, $rootScope) {
  $scope.pages = [];

  ReasonService.get_reasons();
  $rootScope.$on(ReasonService.MORE_REASONS_EVENT, function(evt, data) {
    $scope.pages.push(data.objects);
  });

  $scope.total_items = function() {
    var total = 0;
    _.each($scope.pages, function(p) {
        total = total + p.length;
    });
    return total;
  };
}
