angular.module('ibkg').factory('ReasonService', function($http, $rootScope, $timeout) {
  var MORE_REASONS_EVENT = 'more_reasons_loaded';

  var get_reasons = function(page) {
    if (!page) {
      page = 1;
    }
    $http.get("/api/change_org_reason?results_per_page=20&page=" + page).then(function(resp) {
      $rootScope.$broadcast(MORE_REASONS_EVENT, resp.data);

      if (resp.data.page < resp.data.total_pages) {
      // if (resp.data.page < 2) {
        $timeout(function() {
          get_reasons(resp.data.page + 1);
        }, 500);
      }
    });
  };

  return {
    get_reasons: get_reasons,
    MORE_REASONS_EVENT: MORE_REASONS_EVENT
  };
});
