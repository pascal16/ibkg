module.exports = {
  ng_appname: 'ibkg',

  build_dir: 'build',
  compile_dir: 'dist',
  config_dir: './config/',

  config_files: {
    json: ['config/*.json']
  },
  app_files: {
    js: ['src/app/**/*.js'],

    atpl: ['src/app/**/*.html'],

    less: 'src/less/master.less'
  },

  test_files: {
    js: [
      'bower_components/angular-mocks/angular-mocks.js'
    ]
  },

  vendor_files: {
    js: [
      'bower_components/underscore/underscore.js',
      
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/jquery-resize/jquery.ba-resize.min.js',
      'bower_components/jquery-autosize/jquery.autosize.min.js',
      
      'bower_components/showdown/compressed/showdown.js',
      'bower_components/respond/dest/respond.min.js',
      'bower_components/momentjs/min/moment-with-langs.min.js',
      
      'bower_components/angular/angular.min.js',
      'bower_components/angular-route/angular-route.min.js',
      'bower_components/angular-resource/angular-resource.min.js',
      'bower_components/angular-promise-tracker/promise-tracker.min.js',
      'bower_components/angular-animate/angular-animate.min.js',
      'bower_components/angular-sanitize/angular-sanitize.min.js',

      'bower_components/imagesloaded/imagesloaded.pkgd.min.js',
      'bower_components/masonry/dist/masonry.pkgd.min.js',
      'bower_components/angular-masonry/angular-masonry.js',

      'bower_components/bootstrap/dist/js/bootstrap.min.js',
    ],
    less: [
      'bower_components/bootstrap/less/bootstrap.less'
    ],
    fonts: [
      'bower_components/bootstrap/fonts/*'
    ],
    css: [

    ],
    assets: [
     
    ]
  }
};
