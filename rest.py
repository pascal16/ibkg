import logging

import flask.ext.restless

import flask

from db import *
from server import app, db
# import util

manager = flask.ext.restless.APIManager(app, session=db.session)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# manager.create_api(
#   Author, 
#   methods=['GET', 'POST'],
#   exclude_columns=['password_hash'],
#   preprocessors={ 'POST': [hash_author_password] },
#   postprocessors={ 'POST': [generate_author_token] },
# )

manager.create_api(Author, methods=['GET'])
manager.create_api(ChangeOrgReason, methods=['GET'])
