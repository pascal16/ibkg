import sys

from server import app

import admin
import rest
import routes

if (len(sys.argv) > 1 and sys.argv[1] == "debug"):
  app.run(host="0.0.0.0", port=1711, debug=True)
else:
  app.run(host="0.0.0.0", port=1711)
