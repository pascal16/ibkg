import flask
import flask.ext.sqlalchemy

from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView

from db import *

from server import app, db

admin = Admin(app, url="/admin")

class AuthorModelView(ModelView):
  column_list = ('name', 'change_org_url', 'reasons')
  form_columns = ('name', 'change_org_url', 'reasons')
  def __init__(self, name=None, category=None, endpoint=None, url=None, **kwargs):
    for k, v in kwargs.iteritems():
      setattr(self, k, v)

    super(AuthorModelView, self).__init__(
      Author,
      db.session, 
      name=name, 
      category=category, 
      endpoint=endpoint, 
      url=url
    )

admin.add_view(AuthorModelView())
admin.add_view(ModelView(ChangeOrgReason, db.session))
