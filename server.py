import flask
import db_config
from flask.ext.sqlalchemy import SQLAlchemy

app = flask.Flask(__name__,
  static_url_path="",
)
app.secret_key = 'trollolol'

import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.INFO)

app.config['SQLALCHEMY_DATABASE_URI'] = db_config.connection_string
db = SQLAlchemy(app)
session = db.session
