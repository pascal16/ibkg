import requests
import json
import dateutil.parser

from db import *
from server import session

BASE_URL = "https://api.change.org/v1/petitions/2155574"
REASONS_URL = BASE_URL + "/reasons"
API_KEY = "e12810e7fe810d5eaad7bf5137e094379f4a3a7a5d68ba3337bb72a439c3cee2"

def get_reasons(page):
  print "Getting page %s of reasons" % page
  resp = requests.get(REASONS_URL, params={
    'api_key': API_KEY,
    'page': page
  })
  return resp.json()

def get_all_reasons():
  reasons = []

  page_number = 1
  first_page = get_reasons(page_number)
  reason = reasons + first_page['reasons']
  page_number = page_number + 1
  
  next_page_exists = first_page['next_page_endpoint']
  while next_page_exists:
    page = get_reasons(page_number)
    reasons = reasons + page['reasons']
    page_number = page_number + 1
    next_page_exists = page['next_page_endpoint']

  return reasons

def save_all_reasons():
  session.query(ChangeOrgReason).delete()
  session.query(Author).delete()

  reasons = get_all_reasons()
  for r in reasons:
    author = None
    existing = session.query(Author).get(r['author_name'])
    if existing:
      author = existing
    else:
      author = Author(
        name=r['author_name'],
        change_org_url=r['author_url']
      )
    change_org_reason = ChangeOrgReason(
      author=author,
      content=r['content'],
      created_at=dateutil.parser.parse(r['created_at']),
      like_count=r['like_count']
    )
    # import pdb; pdb.set_trace()
    session.add(author)
    session.add(change_org_reason)
    session.commit()

if __name__ == "__main__":
  save_all_reasons()
