import logging
import os
import uuid
from datetime import datetime, timedelta

from flask import request, render_template, redirect, url_for, jsonify, send_from_directory
import flask

from sqlalchemy import sql

from db import *
from server import app, db

def jsonify_tweets(tweets):
  results = []
  for t in tweets:
    res = t.to_dict()
    res['twitter_user'] = t.twitter_user.to_dict()
    res['hashtags'] = [h.to_dict() for h in t.hashtags]
    res['media_urls'] = [u.to_dict() for u in t.media_urls]
    results.append(res)
  return jsonify(data=results)

@app.route("/api/tweets/popular/<int:party_id>")
def popular_tweets(party_id):
  limit = request.args.get('limit', None)
  hours = int(request.args.get('hours', 12))

  tweets = db.session.query(Tweet)\
    .filter(Tweet.twitter_user.has(party_id=party_id))\
    .filter(Tweet.twitter_timestamp >= (datetime.utcnow() - timedelta(hours=hours)))\
    .order_by(Tweet.retweet_count.desc())
  
  if limit:
    tweets = tweets.limit(limit)
  else:
    tweets = tweets.all()

  return jsonify_tweets(tweets)
  
@app.route("/api/tweets/recent/<int:party_id>")
def recent_tweets(party_id):
  limit = request.args.get('limit', None)
  tweets = db.session.query(Tweet)\
    .filter(Tweet.twitter_user.has(party_id=party_id))\
    .order_by(Tweet.twitter_timestamp.desc())
  
  if limit:
    tweets = tweets.limit(limit)
  else:
    tweets = tweets.all()

  return jsonify_tweets(tweets)
